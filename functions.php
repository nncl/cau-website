<?php
/* WIDGETS */

if (function_exists('register_sidebar'))
{
    register_sidebar(array(
        'name'          => 'Sidebar',
        'before_widget' => '<div class="widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ));
}

/*******************************
Update the login image page
********************************/
function my_custom_login_logo() {
    $blog_path = get_template_directory_uri();
    echo '<style type="text/css">
    	body.login div#login h1 a {

    		background-image: url('.$blog_path.'/site/images/caue-logo.fw.png);
    		width: 100px;
    		-webkit-background-size: 100px 74px;
    		background-size: 100px 74px;
    	}
    </style>';
}

add_action('login_head', 'my_custom_login_logo');

//Link na tela de login para a página inicial
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Cauê Almeida Desenvolvedor Web';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

// CATEGORIES'S AJAX
//
function prefix_load_cat_posts () {
    $cat_id = $_POST[ 'cat' ];
    $args = array (
        'cat' => $cat_id,
        'posts_per_page' => 10,
        'order' => 'DESC'

    );

    global $post;
    $posts = get_posts( $args );

    ob_start ();

    foreach ( $posts as $post ) {
        setup_postdata( $post ); ?>

        <article role="article" itemscope itemtype="http://schema.org/Article">
            <div class="w-col w-col-3 portfolio-row">
                <?php
                    $custom_url = get_post_meta( get_the_ID(), 'Second Excerpt', true );
                ?>
                <a itemprop="url"
                    class="w-inline-block portfolio-photo"
                    href="<?php echo $custom_url; ?>"
                    target="_blank"
                    style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>')">
                    <div class="portfolio-photo-overlay">
                        <div class="portfolio-tittle" data-ix="scroll-fade-out" itemprop="headline">
                            <?php the_title(); ?>
                        </div>
                    </div>
                </a>
            </div>
        </article>

    <?php } wp_reset_postdata();

    $response = ob_get_contents();
    ob_end_clean();

    echo $response;
    die(1);
}

add_action( 'wp_ajax_nopriv_load-filter', 'prefix_load_cat_posts' );
add_action( 'wp_ajax_load-filter', 'prefix_load_cat_posts' );

// Featured image
add_theme_support( 'post-thumbnails' );

// custom fields in posts
//
add_action( 'admin_menu', 'my_create_post_meta_box' );
add_action( 'save_post', 'my_save_post_meta_box', 10, 2 );

function my_create_post_meta_box() {
    add_meta_box( 'my-meta-box', 'Project URL', 'my_post_meta_box', 'post', 'normal', 'high' );
}

function my_post_meta_box( $object, $box ) { ?>
    <p>
        <input
            type="text"
            name="second-excerpt"
            id="second-excerpt"
            placeholder="Ex: http://google.com"
            value="<?php echo wp_specialchars( get_post_meta( $object->ID, 'Second Excerpt', true ), 1 ); ?>"
            style="width: 100%;">
        <input type="hidden" name="my_meta_box_nonce" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
    </p>
    <p>
        <input
            type="text"
            name="third-excerpt"
            id="third-excerpt"
            placeholder="Ex: WordPress Theme development - Freelancer"
            value="<?php echo wp_specialchars( get_post_meta( $object->ID, 'Third Excerpt', true ), 1 ); ?>"
            style="width: 100%;">
        <input type="hidden" name="my_meta_box_nonce_2" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
    </p>
<?php }

function my_save_post_meta_box( $post_id, $post ) {

    if ( !wp_verify_nonce( $_POST['my_meta_box_nonce'], plugin_basename( __FILE__ ) ) )
        return $post_id;

    if ( !wp_verify_nonce( $_POST['my_meta_box_nonce_2'], plugin_basename( __FILE__ ) ) )
        return $post_id;

    if ( !current_user_can( 'edit_post', $post_id ) )
        return $post_id;

    $meta_value = get_post_meta( $post_id, 'Second Excerpt', true );
    $new_meta_value = stripslashes( $_POST['second-excerpt'] );

    $meta_value_2 = get_post_meta( $post_id, 'Third Excerpt', true );
    $new_meta_value_2 = stripslashes( $_POST['third-excerpt'] );

    if ( $new_meta_value && '' == $meta_value )
        add_post_meta( $post_id, 'Second Excerpt', $new_meta_value, true );

    elseif ( $new_meta_value != $meta_value )
        update_post_meta( $post_id, 'Second Excerpt', $new_meta_value );

    elseif ( '' == $new_meta_value && $meta_value )
        delete_post_meta( $post_id, 'Second Excerpt', $meta_value );

    // 3rd field

    if ( $new_meta_value_2 && '' == $meta_value_2 )
        add_post_meta( $post_id, 'Third Excerpt', $new_meta_value_2, true );

    elseif ( $new_meta_value_2 != $meta_value_2 )
        update_post_meta( $post_id, 'Third Excerpt', $new_meta_value_2 );

    elseif ( '' == $new_meta_value_2 && $meta_value_2 )
        delete_post_meta( $post_id, 'Third Excerpt', $meta_value_2 );
}