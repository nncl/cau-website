<?php
	require_once('config.php');
?>

<!DOCTYPE html>

<html lang="pt-br">

	<head>

		<meta charset="UTF-8" />

		<!--[if IE]>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

		<![endif]-->

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Cauê Almeida - Desenvolvedor</title>
		<meta name="description" content="Cauê Almeida - Desenvolvedor" />
		<meta name="keywords" content="desenvolvedor, webdesigner, designer, developer, webdeveloper, back-end developer, front-end developer, desenvolvimento web, wordpress developer, wordpress" />
		<meta name="author" content="Cauê Almeida" />

		<link rel="shortcut icon" href="<?php echo $url_path ?>/site/images/caue-logo.fw.png">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv-printshiv.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

	<body>

		<a href="<?php echo $path ?>/us" title="I prefer english, please" class="ico-flag ico-us"></a>

		<div class="container">

			<div class="bl-main" id="bl-main" onfocus="MM_validateForm('nome','','R','email','','RisEmail','assunto','','R','mensagem','','R');return document.MM_returnValue">

				<section>

					<div class="bl-box">

						<h2 class="bl-icon bl-icon-about">Sobre</h2>

					</div>

					<div class="bl-content">

						<h2>Olá Startups, empresários e empreendedores</h2>

						<p>Meu nome é Cauê e utilizo novas e ótimas tecnologias para desenvolver seus produtos com um belo design e com uma grande performance. Amo codificar, desenvolver e criar projetos em que todos possamos ganhar. Confira meu portfólio e caso desejar, contate-me: <a href="mailto:nncl@live.com">nncl@live.com</a></p>

						<h2>Me encontre nas redes sociais:</h2>

						<p>

							<a href="http://br.linkedin.com/pub/cauê-almeida/63/479/4bb

" target="_blank" title="Me encontre no Linkedin"><strong>&laquo; Linkedin</strong></a>

							<a href="http://github.com/nncl" target="_blank" title="Me encontre no Github"><strong>&laquo; Github</strong></a>
							<a href="http://behance.net/cauealmeida" target="_blank" title="Me encontre no Behance"><strong>&laquo; Behance</strong></a>
						</p>

					</div>

					<span class="bl-icon bl-icon-close"></span>

				</section>

				<section id="bl-work-section">

					<div class="bl-box">

						<h2 class="bl-icon bl-icon-works">Portfólio</h2>

					</div>

					<div class="bl-content">

						<h2>Meus Trabalhos</h2>

						<p>Aqui você vai encontrar diversos dos meus trabalhos de desenvolvimento.</p>

						<ul id="bl-work-items">

							<li data-panel="panel-6"><a href="#"><img src="<?php echo $url_path ?>/site/images/ccm.png" alt="Cabelo Cabelo Meu!" /></a></li>

							<li data-panel="panel-7"><a href="#"><img src="<?php echo $url_path ?>/site/images/cartao.png" alt="Cartões de visita" /></a></li>

							<li data-panel="panel-8"><a href="#"><img src="<?php echo $url_path ?>/site/images/POABV-screen.png" alt="Pra onde as borboletas voam" /></a></li>

							<li data-panel="panel-9"><a href="#"><img src="<?php echo $url_path ?>/site/images/dvl.png" alt="De Laço e Vestido" /></a></li>

						</ul>

					</div>

					<span class="bl-icon bl-icon-close"></span>

				</section>

				<section>

					<div class="bl-box">

						<h2 class="bl-icon bl-icon-blog">Dicas</h2>

					</div>

					<div class="bl-content">
						<?php query_posts('post_id=18'.$id .'&paged='.$paged); ?>
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<h2><?php the_title() ?></h2>
							<?php the_content() ?>
						<?php endwhile  ?>
						<?php else : ?>
						<?php endif; ?>
					</div>

					<span class="bl-icon bl-icon-close"></span>

				</section>

				<section>

					<div class="bl-box">

						<h2 class="bl-icon bl-icon-contact">Contato</h2>

					</div>

					<div class="bl-content">

						<h2>O Prazer é todo meu</h2>

						<p>Se você ficou interessado em algum dos meus projetos, ou gostaria de fazer algum orçamento ou até fazer alguma crítica, preencha os campos a seguir que logo te responderei. <br>Caso queira me contatar por telefone, fique à vontade: <a href="#">11 95205-3515</a></p>

						<form id="Contato" name="Contato" action="<?php echo $url_path ?>/site/enviarEmail.php" method="post">

							<table id="tabela">



								<tr>

									<td><input type="text" id="nome" name="nome" placeholder="Nome"></td>

								</tr>

								<tr>

									<td><input type="text" id="email" name="email" placeholder="E-mail"></td>

								</tr>

								<tr>

									<td><input type="text" id="assunto" name="assunto" placeholder="Assunto"></td>

								</tr>

								<tr>

									<td><textarea id="mensagem" name="mensagem" placeholder="Mensagem"></textarea></td>

								</tr>

								<tr>

									<td><input type="submit" id="enviar" onclick="MM_validateForm('nome','','R','email','','RisEmail','assunto','','R','mensagem','','R');return document.MM_returnValue" value="Enviar"></td>

								</tr>

							</table>

						</form>

					</div>

					<span class="bl-icon bl-icon-close"></span>

				</section>



				<!-- Itens do portfólio -->

				<div class="bl-panel-items" id="bl-panel-work-items">

					<div data-panel="panel-6">

						<div>

							<img src="<?php echo $url_path ?>/site/images/ccm.png" alt="Cabelo Cabelo Meu!" />

							<h3>Cabelo Cabelo Meu!</h3>

							<p>O Cabelo Cabelo Meu! é um blog focado em <strong>cabelos, unhas e maquiagem</strong>, mas que também abordam assuntos como <strong>resenhas</strong> - diversas delas de diversos assuntos - e assuntos gerais. Esse é com certeza um blog para assinar a Newsletter.</p>

							<p>Nesse projeto, fui responsável por desenvolver o <strong>wireframe, design, programação/desenvolvimento,</strong> além da compra de domínio. O blog é também <strong>responsivo</strong>, ou seja, adaptado para os leitores que estão também em tablets e celulares.</p>

							<p>Você pode conferir esse projeto <a href="http://cabelocabelomeu.com" target="_blank" title="Cabelo Cabelo Meu!">aqui</a>.</p>

						</div>

					</div>

					<div data-panel="panel-7">

						<div>

							<img src="<?php echo $url_path ?>/site/images/cartao.png" alt="Cartões de Visita" />

							<h3>Meus cartões de visita</h3>

							<p>Chegaram os meus cartões de visita. Como você pode ver, o design dele é bem clean, e também procurei trabalhar os padrões que chamamos de <a href="http://gizmodo.uol.com.br/o-que-e-flat-design/" title="O que é Flat Design?" target="_blank">flat design</a>, além da semelhança de design com o site. O cartão contém o meu nome, os ícones das tecnologias atuais que trabalho - Javascript, HTML5, WordPress e CSS3 - meu número de celular e meu site.</p>

								<img src="<?php echo $url_path ?>/site/images/cartao-frente.png" alt="Cartão de visita frente Cauê Almeida" />

								<img src="<?php echo $url_path ?>/site/images/cartao-verso.png" alt="Cartão de visita verso Cauê Almeida" />

						</div>

					</div>

					<div data-panel="panel-8">

						<div>

							<img src="<?php echo $url_path ?>/site/images/POABV-screen.png" alt="Pra Onde As Borboletas Voam" />

							<h3>Pra onde as borboletas voam</h3>

							<p>O Pra onde as borboletas voam é um blog de histórias totalmente verídicas, o que muda são os nomes dos personagens - para aqueles que não gostam/querem expor seus nomes. Para as pessoas amantes de livros vale a pena acessar o blog.</p>

							<p>Dessa vez, fui responsável por criar o <strong>logotipo</strong>, <strong>design</strong> e <strong>desenvolvimento/programação</strong> do blog, além da compra de domínio e hospedagem do blog pelo <strong>WordPress.org</strong>. Ressalto que o site é totalmente responsível, sendo acessível em tablets e celulares. Foi um ótimo projeto de se trabalhar.</p>

							<p>Você pode conferir esse projeto <a href="http://praondeasborboletasvoam.com" target="_blank" title="Pra Onde As Borboletas Voam">aqui</a>.</p>

						</div>

					</div>

					<div data-panel="panel-9">

						<div>

							<img src="<?php echo $url_path ?>/site/images/dvl.png" alt="De Laço e Vestido" />

							<h3>De Laço e Vestido</h3>

							<p>O De Laço e Vestido é o blog da Samara Pupo, onde ela diz tudo sobre ela e sobre o que gosta. É um blog perfeito para mulheres. Lá tem muita coisa sobre maquiagens, looks e muito mais. Vale a pena assinar a newsletter.</p>

							<p>Fui responsável por desenvolver o wireframe, layout e programação do blog. Inicialmente, o blog estava hospedado no WordPress.com, então fui responsável também pela mudança para o WordPress.org.</p>

							<p>Você pode conferir esse projeto <a href="http://delacoevestido.com" target="_blank" title="De Laço e Vestido">aqui</a>.</p>

						</div>

					</div>

					<nav>

						<span class="bl-next-work">&gt; Próximo Projeto</span>

						<span class="bl-icon bl-icon-close"></span>

					</nav>

				</div>

			</div><!-- FIM Itens do portfólio -->



		<div id="footer">

			<?php wp_footer(); ?>

		</div>



		</div><!-- /container -->

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

		<script src="<?php echo $url_path ?>/site/js/boxlayout.js"></script>

		<script>

			$(function() {

				Boxlayout.init();

			});

		</script>



        <script type="text/javascript">

			function MM_validateForm() { //v4.0

			  if (document.getElementById){

			    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;

			    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);

			      if (val) { nm=val.name; if ((val=val.value)!="") {

			        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');

			          if (p<1 || p==(val.length-1)) errors+='- '+nm+' deve conter um endereço de E-mail.\n';

			        } else if (test!='R') { num = parseFloat(val);

			          if (isNaN(val)) errors+='- '+nm+' deve conter um número.\n';

			          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');

			            min=test.substring(8,p); max=test.substring(p+1);

			            if (num<min || max<num) errors+='- '+nm+' deve conter um número entre '+min+' e '+max+'.\n';

			      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' é obrigatório.\n'; }

			    } if (errors) alert('Ocorreu(ram) o(s) seguinte(s) erro(s):\n'+errors);

			    document.MM_returnValue = (errors == '');

			} }

		</script>

	</body>

</html>
