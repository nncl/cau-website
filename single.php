<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8" />
        <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
        <meta name="description" content="Cauê Almeida - Desenvolvedor" />
        <meta name="keywords" content="desenvolvedor, webdesigner, designer, developer, webdeveloper, back-end developer, front-end developer, desenvolvimento web, wordpress developer, wordpress" />
        <meta name="author" content="Cauê Almeida" />

        <meta property="og:image" content="<?php echo $url_path ?>/site/images/caue-logo.fw.png">

        <link rel="shortcut icon" href="<?php echo $url_path ?>/site/images/caue-logo.fw.png">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $url_path ?>/site/lib/ladda/dist/ladda.min.css">

        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

        <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv-printshiv.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php wp_head(); ?>
    </head>
    <body itemscope itemtype="http://schema.org/WebPage" class="us-page ca-post">
        <article
            itemscope itemtype="http://schema.org/Article"
            class="ca-item">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <header class="ca-post-header">
                    <?php
                        for ($i=0; $i < 3; $i++) :
                    ?>
                    <div class="w-col w-col-3 portfolio-row">
                        <a itemprop="url"
                            class="w-inline-block portfolio-photo"
                            style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>')">
                            <div class="portfolio-photo-overlay">
                                <div class="portfolio-tittle" data-ix="scroll-fade-out" itemprop="headline">
                                    <h1 role="banner"><?php the_title(); ?></h1>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php endfor; ?>
                </header>

                    <?php
                        // $meta = get_post_meta( get_the_ID() );
                        $meta = get_post_meta( get_the_ID(), 'Second Excerpt', true );
                    ?>

                <div class="ca-ck">
                    <?php the_content(); ?>
                </div>

                <footer>
                    <a href="<?php echo site_url(); ?>" class="ca-back" title="Back to home">
                        Back to home
                    </a>
                </footer>
            <?php endwhile; else: ?>
                <li>
                    <h2>Sorry</h2>
                </li>
            <?php endif; ?>
        </article>
    </body>
</html>
