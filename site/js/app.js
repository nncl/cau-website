function scroll_effect() {
  $(".scroll").click(function(event){
    event.preventDefault();
    $('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
    $('body').css('overflow' , 'auto');
    $('.big-menu').fadeOut('fast');
  });
}

function menu() {
  $('#main-menu').on('click' , function() {
    $('body').css('overflow' , 'hidden');
    $('.big-menu').fadeIn('fast');
  });

  $('.close').on('click' , function() {
    $('body').css('overflow' , 'auto');
    $(this).parent().fadeOut('fast');
  });
}

function contact() {
  $('#call-form').on('click' , function() {
    $('body').css('overflow' , 'hidden');
    $('#contact-form').fadeIn('fast');
  });

  $('.close').on('click' , function() {
    $('body').css('overflow' , 'auto');
    $(this).parent().fadeOut('fast');
  });
}

function send() {
  $('#send').on('click' , function() {

    var form = $('#ctc-form');
    var formMessages = $('.response');

    form.submit(function () {
      var user_name = $('#username').val();
      var user_mail = $('#usermail').val();
      var msg = $('#message').val();

      $.ajax(
        {
          type: "POST",
          url: "https://mandrillapp.com/api/1.0/messages/send.json",
          data: {
            'key': 'u6q142m0VE9amASUCtBdWQ',
            'message': {
              'from_email': user_mail,
              'from_name': user_name,
              'headers': {
                'Reply-To': user_name
              },
              'subject': 'Cauê Website - Contato de ' + user_name,
              'text': msg,
              'to': [
                {
                  'email': 'ctropiani@yahoo.com',
                  'name': 'Caue',
                  'type': 'to'
                }]
            }
          },
          beforeSend: function(){
            $("#send").hide();
            $(".loading").css('display' , 'block');
          }
        })

        .done(function(response) {
          $(".loading").hide();

          $(formMessages).text("Your message has been sent! So soon I'll aswer you.");

          form.trigger('reset');
        })

        .fail(function(data) {
          $(".load").hide();

          $(formMessages).removeClass('success');
          $(formMessages).addClass('error');

          // Enviando a mensagem
          if ( data.response !== '' ) {
            $(formMessages).text( 'Ops!! Are you sure you fill all the form?' );
          } else {
            $(formMessages).text( 'Ops! Something went wrong, sorry. Try again in some minutes.' );
          }

        });
      return false; // prevent page refresh
    });

  });
}

$(document).ready(function() {
  menu();
  scroll_effect();
  // send();
  contact();
});
