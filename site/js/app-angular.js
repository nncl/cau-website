var app = angular.module('caueApp', [
  'angular-ladda'
]);

app.controller('AppCtrl', function($scope, AppService){
  $scope.user = {};
  $scope.feedback = "";
  $scope.submitting = false;

  $scope.send = function(form) {
    if (form.$valid) {
      $scope.submitting = true;

      AppService.send($scope.user).then(
        function success(res) {
          $scope.submitting = false;
          $scope.feedback = "Your message has been sent. We'll talk soon :)";
          $scope.user = {};
          form.$setPristine();
          console.log(res);
        },
        function error(err) {
          $scope.submitting = false;
          $scope.feedback = "Oops!! Something went wrong :( Try again!!";
          console.log('Error');
          console.log(err);
        }
      );
    } else {
    console.log('Invalid form');
    }
  }
});

app.service('AppService', function($q, $http){
  var self = {
    'serializeObj': function (obj) {
        var result = [];

        for (var property in obj)
            result.push(decodeURIComponent(property) + "=" + decodeURIComponent(obj[property]));

        return result.join("&");
    },
    'send': function (form) {
        var d = $q.defer();

        var obj = self.serializeObj(form);

        $http({
            url: '/wp-content/themes/caue-website/send.php',
            method: "POST",
            cache: false,
            data: obj,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {
            d.resolve(data);

        }).error(function (data, status, headers, config) {
            d.reject(data);
        });

        return d.promise;
    }
  }

  return self;
})