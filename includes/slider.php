

	<button type="button" id="main-menu" class="fa fa-bars cross"></button>

	<div class="big-menu light">
		<button type="button" class="reset-btn fa fa-times close cross"></button>

		<div class="menu-box">
			<div class="all-center">
				<div class="center">
					<nav role="navigation">
						<ul class="main-menu">
							<li>
								<a href="#about" title="About" class="scroll cross">About</a>
							</li>

							<li>
								<a href="#portfolio" title="Portfolio" class="scroll cross">Portfolio</a>
							</li>

							<li>
								<a href="#blog" title="Blog" class="scroll cross">Blog</a>
							</li>

							<li>
								<a href="#contact" title="Contact" class="scroll cross">Contact</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div> <!-- lightbox menu -->

	<!-- SLIDER -->
	<div id="wrap" class="wrap">
		<div class="mockup">
			<img class="mockup__img" src="<?php echo $url_path ?>/site/images/mockup4.jpg" />
			<div class="mobile">
				<ul id="slideshow-1" class="slideshow">
					<li class="slideshow__item"><img src="<?php echo $url_path ?>/site/images/small/1.png"/></li>
					<li class="slideshow__item"><img src="<?php echo $url_path ?>/site/images/small/2.png"/></li>
					<li class="slideshow__item"><img src="<?php echo $url_path ?>/site/images/small/3.png"/></li>
					<li class="slideshow__item"><img src="<?php echo $url_path ?>/site/images/small/4.png"/></li>
				</ul>
			</div>
			<div class="screen">
				<ul id="slideshow-2" class="slideshow">
					<li class="slideshow__item current"><img src="<?php echo $url_path ?>/site/images/large/1.png"/></li>
					<li class="slideshow__item"><img src="<?php echo $url_path ?>/site/images/large/2.png"/></li>
					<li class="slideshow__item"><img src="<?php echo $url_path ?>/site/images/large/3.png"/></li>
					<li class="slideshow__item"><img src="<?php echo $url_path ?>/site/images/large/4.png"/></li>
				</ul>
			</div>
			<header class="codrops-header">
				<h1>
					<span>Hello</span>
					Nice to meet you!
					
					<i>I'm Cauê - you can call me Bruno as well - and I'm a full stack developer. <br/>
						<a>What else?</a>
					</i>
				</h1>
			</header>
		</div>
	</div>
	<!-- END SLIDER -->
