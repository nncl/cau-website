
<?php
	require_once('config.php');
?>

<!doctype html>
<html>
	<head>

		<meta charset="UTF-8" />

		<!--[if IE]>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

		<![endif]-->

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Cauê Almeida - Desenvolvedor</title>
		<meta name="description" content="Cauê Almeida - Desenvolvedor" />
		<meta name="keywords" content="desenvolvedor, webdesigner, designer, developer, webdeveloper, back-end developer, front-end developer, desenvolvimento web, wordpress developer, wordpress" />
		<meta name="author" content="Cauê Almeida" />

		<link rel="shortcut icon" href="<?php echo $url_path ?>/site/images/caue-logo.fw.png">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" />

		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

		<!--[if lt IE 9]>
		<script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv-printshiv.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

	<body itemscope itemtype="http://schema.org/WebPage" class="not-found">
		<h1 itemprop="name">Sorry</h1>
		<h2 itemprop="description">We could not find what you're looking for.</h2>
		<a href="/">Back to home</a>
	</body>
</html>
