
<?php
	require_once('config.php');
?>

<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<!--[if IE]>
			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Cauê Almeida - Full Stack Developer</title>
		<meta name="description" content="Cauê Almeida - Full Stack Developer" />
		<meta name="keywords" content="desenvolvedor, webdesigner, developer, webdeveloper, back-end developer, front-end developer, web development, wordpress developer, wordpress, app developer, apps, MEAN STACK" />
		<meta name="author" content="Cauê Almeida" />

		<meta property="og:image" content="<?php echo $url_path ?>/site/images/caue-logo.fw.png">

		<link rel="shortcut icon" href="<?php echo $url_path ?>/site/images/caue-logo.fw.png">
        <link href='//cdn.jsdelivr.net/devicons/1.8.0/css/devicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>?v=234908409823908423" />
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $url_path ?>/site/lib/ladda/dist/ladda.min.css">

		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv-printshiv.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<?php wp_head(); ?>
	</head>

	<body itemscope itemtype="http://schema.org/WebPage" class="us-page" ng-app="caueApp" ng-controller="AppCtrl">
		<section>

			<?php include_once('includes/slider.php'); ?>

			<section id="about" class="sec">
				<article itemscope itemtype="http://schema.org/Article">
					<header>
						<h1 itemprop="headline" class="title">What I'm doing?</h1>
					</header>

					<p itemprop="text">I've a degree in Systems Development and Analysis, and now I'm a <a href="https://www.fiap.com.br/mba/mba-em-desenvolvimento-de-aplicacoes-e-games-para-dispositivos-moveis-internet-das-coisas/" target="_blank">MBA</a> student.</p>

					<h2 itemprop="text" class="subtitle c--knowledge">More than knowledgement, this is how I live.</h2>

					<nav role="navigation">
						<ul class="tech">
							<li>
                                <i class="devicons devicons-mongodb"></i>
							</li>

							<li>
                                <i class="devicons devicons-angular"></i>
							</li>

							<li>
                                <i class="devicons devicons-nodejs_small"></i>
							</li>

							<li>
                                <i class="devicons devicons-ionic"></i>
							</li>

							<li>
                                <i class="devicons devicons-sass"></i>
							</li>

							<li>
                                <i class="devicons devicons-wordpress"></i>
							</li>
						</ul>
					</nav>

				</article>
			</section>

			<section id="portfolio">
				<article itemscope itemtype="http://schema.org/Article">
					<header>
						<h1 itemprop="headline">Wanna check some work? Here it goes.</h1>
					</header>

					<h2 itemprop="text" class="has-block-spn">
						<span>By analysing each study case of each client, we could build an amazing user experience</span>
						<span>joined with performance, security and accessibility.</span>
					</h2>

					<?php
						$categories = get_categories();
					?>

					<nav>
						<ul class="ca-categories-list">
							<?php foreach ($categories as $cat): ?>
								<li>
									<a
										id="cat-<?php echo $cat->term_id; ?>"
										class="<?php echo $cat->slug; ?> ca-ajax"
										data-cat="<?php echo $cat->term_id ?>">
										<?php echo $cat->name; ?>
									</a>
								</li>
							<?php endforeach ?>
						</ul>
					</nav>

					<div id="ca-loading-animation" style="display: none;">
					  <img
					    style="width: 50px"
					    src="<?php echo $url_path ?>/site/images/01-progress.gif"/>
					</div>

					<div
						id="ca-category-post-content"
						class="w-row work-row">

						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					        <article role="article" itemscope itemtype="http://schema.org/Article">
					            <div class="w-col w-col-3 portfolio-row">
					            	<?php
		                            	$custom_url = get_post_meta( get_the_ID(), 'Second Excerpt', true );
		                            	$custom_desc = get_post_meta( get_the_ID(), 'Third Excerpt', true );
		                            ?>
					                <a itemprop="url"
					                    class="w-inline-block portfolio-photo"
					                    href="<?php echo $custom_url; ?>"
					                    target="_blank"
					                    style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>')">
					                    <div class="portfolio-photo-overlay">
					                        <div class="portfolio-tittle" data-ix="scroll-fade-out" itemprop="headline">
					                            <?php the_title(); ?>
                                                <h3 class="dd-title dd-title--job">
                                                    <?php echo $custom_desc; ?>
                                                </h3>
					                        </div>
					                    </div>
					                </a>
					            </div>
					        </article>
						<?php endwhile; else : ?>
						<?php endif; ?>

					</div>
			</section><!-- end work section -->

			<section id="blog">
				<h2>Blog</h2>
				<a class="m-profile" href="https://medium.com/@nncl">Cauê Almeida</a>
			</section>

			<footer id="contact" class="sec normal">
				<article itemscope itemtype="http://schema.org/Organization">
					<header>
						<h1 itemprop="headline">Did you like what you've seen? I really hope so.</h1>
					</header>

					<h2 itemprop="text">If you want to talk with me, make it simple: <a id="call-form" class="cross">click here</a> and let's talk about whatever it is.</h2> <!-- button lighbox for a form -->
					<p itemprop="text">It will be always a pleasure talking to you.</p>
					<nav role="navigation">
						<ul class="socials">
							<li>
								<a itemprop="sameAs" href="http://twitter.com/calmeidao" target="_blank" class="fa fa-twitter"></a>
							</li>

							<li>
								<a itemprop="sameAs" href="http://facebook.com/cauedaday" target="_blank" class="fa fa-facebook"></a>
							</li>

							<li>
								<a itemprop="sameAs" href="http://instagram.com/clmeida" target="_blank" class="fa fa-instagram"></a>
							</li>

							<li>
								<a itemprop="sameAs" href="http://br.linkedin.com/pub/cau%C3%AA-almeida/63/479/4bb" target="_blank" class="fa fa-linkedin"></a>
							</li>

							<li>
								<a itemprop="sameAs" href="http://github.com/nncl" target="_blank" class="fa fa-github"></a>
							</li>
						</ul>
					</nav>
				</article>

				<section id="contact-form" class="light">
					<button type="button" class="reset-btn fa fa-times close cross"></button>

					<form id="ctc-form"
						   ng-submit="send(contactForm)"
						   name="contactForm"
						   novalidate="novalidate">
						<div class="contact-box">
							<div class="all-center">
								<div class="center">

									<input type="text" placeholder="Your name*"
										ng-model="user.name"
										name="username"
										required="required"
										ng-class="
											{
												'ca-has-error' : !contactForm.username.$valid && (!contactForm.$pristine || contactForm.$submitted)
											}
										" />

									<input type="email"
										placeholder="Your e-mail*"
										required="required"
										ng-model="user.email"
										name="email"
										ng-class="
											{
												'ca-has-error' : !contactForm.email.$valid && (!contactForm.$pristine || contactForm.$submitted)
											}
										" />

									<input type="text"
										placeholder="What's going on?*"
										required="required"
										ng-model="user.subject"
										name="subject"
										ng-class="
											{
												'ca-has-error' : !contactForm.subject.$valid && (!contactForm.$pristine || contactForm.$submitted)
											}
										" />

									<textarea
										required
										placeholder="Say hello!"
										required="required"
										ng-model="user.message"
										name="message"
										ng-class="
											{
												'ca-has-error' : !contactForm.message.$valid && (!contactForm.$pristine || contactForm.$submitted)
											}
										" ></textarea>

									<button type="submit"
										class="cross ladda-button"
										ladda="submitting"
										data-style="expand-right">
											<span ng-show="!submitting">Hello!</span>
											<span ng-show="submitting">Sending...</span>
										</button>

									<span class="response">
										<br/>
										{{feedback}}
									</span>

								</div>
							</div>
						</div>
					</form>
				</section>

			</footer>

			<div class="end">
				<p role="contentinfo">Made with <span></span></p>
			</div>

			<?php wp_footer(); ?>
		</section>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
		<script src="<?php echo $url_path ?>/site/lib/ladda/dist/spin.min.js"></script>
		<script src="<?php echo $url_path ?>/site/lib/ladda/dist/ladda.min.js"></script>
		<script src="<?php echo $url_path ?>/site/lib/angular-ladda/dist/angular-ladda.min.js"></script>
		<script type="text/javascript" src="<?php echo $url_path ?>/site/js/app-angular.js"></script>
		<script type="text/javascript" src="<?php echo $url_path ?>/site/js/app.js"></script>
		<script type="text/javascript" src="<?php echo $url_path ?>/site/js/script.js"></script>
		<script async src="https://static.medium.com/embed.js"></script>

		<script>
	      const SETTINGS = {
	        rebound: {
	          tension: 16,
	          friction: 5
	        },
	        spinner: {
	          id: 'spinner',
	          radius: 90,
	          sides: 3,
	          depth: 4,
	          colors: {
	            //background: '#231E76',
	            //stroke: '#231E76',
	            background: '#f0f0f0',
	            stroke: '#272633',
	            base: null,
	            child: '#272633'
	          },
	          alwaysForward: true, // When false the spring will reverse normally.
	          restAt: 0.5, // A number from 0.1 to 0.9 || null for full rotation
	          renderBase: false
	        }
	      };
		</script>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-64206709-1', 'auto');
		  ga('send', 'pageview');

		</script>
		<script type="text/javascript" src="<?php echo $url_path ?>/site/js/load.js"></script>

		<script>
            function cat_ajax_get() {
                $('.ca-ajax').on('click' , function() {
                	var postArea = $("#ca-category-post-content");

                    var catID = $(this).attr('data-cat');

                    $(".ca-ajax").removeClass("ca-active");
                    $("#cat-" + catID).addClass("ca-active");
                    $("#ca-loading-animation").slideToggle('fast');
                    var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); //must echo it ?>';

                    $.ajax({
                      type: 'POST',
                      url: ajaxurl,
                      data: {"action": "load-filter", cat: catID },
                      success: function(response) {
                      	postArea.empty();
                        postArea.html(response);
                        $("#ca-loading-animation").slideToggle('fast');
                        return false;
                      }
                    });
                });
            }

            cat_ajax_get();
        </script>
	</body>
</html>
